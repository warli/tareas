<?php
class Create_database
{
    protected $pdo;
    
    public function __construct()
    {
        $this->pdo = new PDO("mysql:host=localhost;", "root", "luisft16");
    }
    //creamos la base de datos y las tablas que necesitemos
    public function susesionFibonacci()
    {
        //creamos la base de datos si no existe
        $crear_db = $this->pdo->prepare('CREATE DATABASE IF NOT EXISTS examen COLLATE utf8_spanish_ci');
        $crear_db->execute();
        
        //decimos que queremos usar la tabla que acabamos de crear
        if($crear_db) {
            echo "Nueva base de datos creada correctamente <br>";
            $use_db = $this->pdo->prepare('USE examen');                          
            $use_db->execute();
        } else {
            echo "No se a podido crear la base de datos: <br>";
        }
        
        //si se ha creado la base de datos y estamos en uso de ella creamos las tablas
        if($use_db) {
            //creamos la tabla usuarios
            $crear_tb_fibonacci = $this->pdo->prepare('
                CREATE TABLE IF NOT EXISTS fibonacci (
                susesion int(7)
                )');                            
            $crear_tb_fibonacci->execute();
            
            //insertamos datos
            $a = 0;
            $b = 1;

            for ($i = 0; $i < 32; $i++) { 
                $fibonacci = $a + $b;
                $insertar = $this->pdo->prepare('INSERT INTO fibonacci (susesion) VALUES (:fibo)');
                $insertar->bindValue(':fibo', $fibonacci, PDO::PARAM_INT);
                $insertar->execute();
                echo $fibonacci . '<br>';
                $a = $b;
                $b = $fibonacci;
                if ($fibonacci%2==0) {
                    $pares[] =$fibonacci;
                }
            }
            echo 'Se terminó de crear una serie de fibonacci <br>';
            $suma_pares = array_sum($pares);
            echo 'La suma de los términos pares es: ' . $suma_pares .'<br>';
        }

        $crearDB = $this->pdo->prepare('
            CREATE TABLE IF NOT EXISTS pares (suma int(15))');
        $resultado = $crearDB->execute();

        if ($resultado) {
            echo 'Se creó una nueva tabla <br>';
        } else {
            echo 'No se ha podido crear una segunda tabla <br>';
        }

        $insertar = $this->pdo->prepare('INSERT INTO pares (suma) VALUES (:suma_pares)');
        $insertar->bindValue(':suma_pares', $suma_pares, PDO::PARAM_INT);
        $resulInsert = $insertar->execute();

        if ($resulInsert) {
            echo 'Se ha insertado la suma de los números pares en la segunda tabla <br>';
        } else {
            echo 'No se ha podido insertar los valores en la segunda tabla ';
        }
        
    }

}
//ejecutamos la función susesionFibonacci para crear nuestra bd y las tablas
$db = new Create_database();
$db->susesionFibonacci();
?>

