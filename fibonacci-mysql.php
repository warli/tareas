<?php

    //include ('conexion.php');

    $servidor = 'localhost';
    $db_usuario = 'root';
    $db_contra = 'luisft16';

    $conexion = mysqli_connect($servidor, $db_usuario, $db_contra);

    if (mysqli_connect_errno()) {
        echo 'Fallo al conectar con mysql <br>';
        exit();
    }
    
    $db = 'CREATE DATABASE examen';

    $resultado = mysqli_query($conexion, $db);

    if ($resultado) {
        echo "Nueva base de datos creada correctamente <br>";
    } else {
        echo "No se a podido crear la base de datos por el siguiente error: <br>". mysqli_error($conexion);
    }

    mysqli_select_db($conexion, 'examen') or die('No se encuentra la base de datos <br>');

    $tabla = 'CREATE TABLE fibonacci (susesion int(7))';


    $resultado_tabla = mysqli_query($conexion, $tabla);
    
    if ($resultado_tabla) {
        echo 'Se ha creado una tabla <br>';
    } else {
        echo 'No se ha podido crear una tabla <br>';
    }

    $a = 0;
    $b = 1;

    for ($i = 0; $i < 32; $i++) { 
        $fibonacci = $a + $b;
        $insert = "INSERT INTO fibonacci (susesion) VALUES ($fibonacci)";
        echo $fibonacci . '<br>';
        $a = $b;
        $b = $fibonacci;
        mysqli_query($conexion, $insert);
        if ($fibonacci%2==0) {
            $pares[] =$fibonacci;
        }
    }

    echo 'Se terminó de crear una serie de fibonacci <br>';
    $suma_pares = array_sum($pares);
    echo 'La suma de los términos pares es: ' . $suma_pares .'<br>';

    $tabla_pares = 'CREATE TABLE pares (suma int(15))';
    $resultado_pares = mysqli_query($conexion, $tabla_pares);

    if ($resultado_pares) {
        echo 'se creó una nueva tabla <br>';
    } else {
        echo 'no se ha podido crear una segunda tabla <br>';
    }
    
    $insert_sum = "INSERT INTO pares (suma) VALUES ($suma_pares)";
    $resultado_segunda_tabla = mysqli_query($conexion, $insert_sum);

    if ($resultado_segunda_tabla) {
        echo 'Se ha insertado la suma de los números pares en la segunda tabla <br>';
    } else {
        echo 'No se ha podido insertar los valores en la segunda tabla ';
    }

    mysqli_close($conexion);
